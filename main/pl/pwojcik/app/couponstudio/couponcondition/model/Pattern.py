#
#
# Entity representing single Pattern standing as coupon's destination.
#
#


class Pattern:
    def __init__(self, patternId, mapping):
        self.patternId = patternId
        self.mapping = mapping

    @property
    def getPatternId(self):
        return self.patternId

    @property
    def getMapping(self):
        return self.mapping
