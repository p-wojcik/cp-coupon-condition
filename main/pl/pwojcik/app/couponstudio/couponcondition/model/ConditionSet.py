from typing import List

from main.pl.pwojcik.app.couponstudio.couponcondition.model.Pattern import Pattern

#
#
# Entity representing set of Patterns extended with name, description & tenant name.
#
#
PatternList = List[Pattern]


class ConditionSet:
    def __init__(self, name='', description='', tenant='', patterns: PatternList = None):
        self.name = name
        self.description = description
        self.tenant = tenant
        self.patterns = patterns

    @property
    def get_name(self):
        return self.name

    @property
    def get_description(self):
        return self.description

    @property
    def get_tenant(self):
        return self.tenant

    @property
    def get_patterns(self):
        return self.patterns
