from sanic import Blueprint
from sanic.response import HTTPResponse

from main.pl.pwojcik.app.couponstudio.couponcondition.exception.ConditionSetValidationException import \
    ConditionSetValidationException
from main.pl.pwojcik.app.couponstudio.couponcondition.service.ConditionSetService import ConditionSetService

from main.pl.pwojcik.app.couponstudio.couponcondition.web.RequestInterceptor import RequestInterceptor
from main.pl.pwojcik.app.couponstudio.couponcondition.config.ApplicationConfig import APP_URL_PREFIX

#
#
# Blueprint responsible for /conditionsets endpoints
#
#

# Variables
conditionSetBlueprint = Blueprint('conditionSet', url_prefix='/<tenant>/conditionsets')
service = ConditionSetService()


@conditionSetBlueprint.route('/', methods=['POST'])
@RequestInterceptor.tenant_aware
@RequestInterceptor.json_aware
async def save_condition_set(request, tenant):
    try:
        entity_id = service.save_condition_set(request.json, tenant)
        location_header = prepare_location_header(request, entity_id)
        return HTTPResponse(status=201, headers={'Location': location_header})
    except ConditionSetValidationException as exception:
        return HTTPResponse(exception.get_error_message(), status=400)


def prepare_location_header(request, entity_id):
    return '{}://{}{}{}/{}'.format(request.scheme, request.host, APP_URL_PREFIX, request.path, entity_id)


@conditionSetBlueprint.route('/<identifier>')
@RequestInterceptor.tenant_aware
@RequestInterceptor.id_aware
async def get_condition_set_by_id(request, tenant, identifier):
    entity = service.get_condition_set_by_id(tenant, identifier)
    if entity is None:
        return HTTPResponse(status=404)
    else:
        return HTTPResponse(entity, content_type='application/json')


@conditionSetBlueprint.route('/')
@RequestInterceptor.tenant_aware
async def get_all_condition_sets(request, tenant):
    entities = service.get_all_condition_sets(tenant)
    return HTTPResponse(entities, content_type='application/json')


@conditionSetBlueprint.route('/<identifier>', methods=['DELETE'])
@RequestInterceptor.tenant_aware
@RequestInterceptor.id_aware
async def delete_condition_set(request, tenant, identifier):
    service.delete_by_id(tenant, identifier)
    return HTTPResponse(status=204, content_type='application/json')
