from sanic.response import HTTPResponse
from main.pl.pwojcik.app.couponstudio.couponcondition.config.ApplicationConfig import CP_TENANT_HEADER
from bson.objectid import ObjectId
from bson.errors import InvalidId


#
#
# Class containing methods that should be used before requests to validate
# required headers & other request parameters.
#


class RequestInterceptor:

    # Checks whether header "Content-Type" is equal to "application/json"
    @staticmethod
    def json_aware(func):
        def wrapper(*args, **kwargs):
            request = args[0]
            if request.content_type != 'application/json':
                return HTTPResponse("Content-type must be 'application/json'", status=415)
            else:
                return func(*args, **kwargs)

        return wrapper

    # Checks whether ID given as path parameter is valid ObjectID
    @staticmethod
    def id_aware(func):
        def wrapper(*args, **kwargs):
            identifier = str(kwargs['identifier'])
            try:
                ObjectId(identifier)
                return func(*args, **kwargs)
            except (TypeError, InvalidId):
                return HTTPResponse('Given identifier is not valid UUID', status=400)

        return wrapper

    # Checks whether given path tenant and header tenant are present and equal
    @staticmethod
    def tenant_aware(func):
        def wrapper(*args, **kwargs):
            request = args[0]
            path_tenant = str(kwargs['tenant'])
            if CP_TENANT_HEADER in request.headers:
                header_tenant = request.headers[CP_TENANT_HEADER]
                if not str(header_tenant):
                    return HTTPResponse('Missing header {}'.format(CP_TENANT_HEADER), status=401)
                elif path_tenant != header_tenant:
                    return HTTPResponse('Path tenant is not equal to header tenant', status=401)
                else:
                    return func(*args, **kwargs)
            else:
                return HTTPResponse('Missing header {}'.format(CP_TENANT_HEADER), status=401)

        return wrapper
