from sanic import Blueprint
from sanic.response import HTTPResponse
from main.pl.pwojcik.app.couponstudio.couponcondition.repository.PatternsRepository import PATTERNS
import json

#
#
# Blueprint responsible for /patterns endpoint. Serves static json file content.
#
#

patternsBlueprint = Blueprint('patterns', url_prefix='/patterns')


@patternsBlueprint.route('/')
async def get_all_patterns(request):
    return HTTPResponse(json.dumps(PATTERNS), content_type='application/json')
