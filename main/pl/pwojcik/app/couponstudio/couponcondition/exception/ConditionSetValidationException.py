#
#
# Exception indicating that validation of Condition Set went wrong.
#
#


class ConditionSetValidationException(BaseException):
    def __init__(self, error_message):
            self.__error_message = error_message

    def get_error_message(self):
        return self.__error_message
