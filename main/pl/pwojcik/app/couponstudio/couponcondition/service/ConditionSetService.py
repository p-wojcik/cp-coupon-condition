from main.pl.pwojcik.app.couponstudio.couponcondition.repository.ConditionSetRepository import ConditionSetRepository
from main.pl.pwojcik.app.couponstudio.couponcondition.validation.ConditionSetValidator import ConditionSetValidator
from main.pl.pwojcik.app.couponstudio.couponcondition.model.ConditionSet import ConditionSet

# Constants
ID_PROPERTY = '_id'
TENANT_PROPERTY = 'tenant'


class ConditionSetService:
    def __init__(self):
        self.__validator = ConditionSetValidator()
        self.__repository = ConditionSetRepository()

    # Save condition set
    def save_condition_set(self, request_json, tenant):
        self.remove_tenant_from_payload(request_json)
        payload = request_json or {}
        entity = ConditionSet(**payload, tenant=tenant)
        self.__validator.validate(entity)
        return self.__repository.save(entity)

    @staticmethod
    def remove_tenant_from_payload(request_json):
        if (request_json is not None) and (TENANT_PROPERTY in request_json):
            del request_json[TENANT_PROPERTY]
        return request_json

    # Get condition set by ID
    def get_condition_set_by_id(self, tenant, identifier):
        result = self.__repository.get_by_id(tenant, identifier)
        self.remove_tenant_and_id_from_payload(result)
        return result

    @staticmethod
    def remove_tenant_and_id_from_payload(result):
        if result is not None:
            del result[ID_PROPERTY]
            del result[TENANT_PROPERTY]

    # Get all condition sets for tenant
    def get_all_condition_sets(self, tenant):
        def rename_id(e):
            object_id = e[ID_PROPERTY]
            e['id'] = str(object_id)
            del e[ID_PROPERTY]
            return e

        entities = self.__repository.get_all(tenant)
        no_tenant = list(self.remove_tenant_from_payload(n) for n in entities)
        return list(rename_id(n) for n in no_tenant)

    # Delete condition set by ID
    def delete_by_id(self, tenant, identifier):
        self.__repository.delete_by_id(tenant, identifier)
