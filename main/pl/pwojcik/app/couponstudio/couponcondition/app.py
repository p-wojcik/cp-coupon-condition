from sanic import Sanic
from sanic_dispatcher import SanicDispatcherMiddlewareController

from main.pl.pwojcik.app.couponstudio.couponcondition.config import ApplicationConfig
from main.pl.pwojcik.app.couponstudio.couponcondition.web.PatternsBlueprint import patternsBlueprint
from main.pl.pwojcik.app.couponstudio.couponcondition.web.ConditionSetBlueprint import conditionSetBlueprint

#
#
# Application entry point
#
#

# Initialize application
rootApp = Sanic(__name__)
app = Sanic("Coupon condition service")

# Register Blueprint
app.blueprint(patternsBlueprint)
app.blueprint(conditionSetBlueprint)

# Dispatcher middleware
dispatcher = SanicDispatcherMiddlewareController(rootApp)
dispatcher.register_sanic_application(app, ApplicationConfig.APP_URL_PREFIX, apply_middleware=True)

# Run application
if __name__ == ApplicationConfig.MAIN_APP_NAME:
    rootApp.run(port=ApplicationConfig.APP_PORT, debug=True)
