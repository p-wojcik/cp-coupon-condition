from main.pl.pwojcik.app.couponstudio.couponcondition.repository.MongoConnector import mongo_connector
from bson.objectid import ObjectId

#
#
# Class responsible for communicating with MongoDB using objects of class ConditionSet
#
#

COLLECTION_NAME = 'couponconditions'


class ConditionSetRepository:
    def __init__(self):
        self.__collection = mongo_connector[COLLECTION_NAME]

    # Saves condition set in database
    def save(self, entity):
        return self.__collection.insert_one(vars(entity)).inserted_id

    # Gets condition set by ID
    def get_by_id(self, tenant, identifier):
        query = {
            '_id': ObjectId(identifier),
            'tenant': tenant
        }
        return self.__collection.find_one(query)

    # Gets condition sets for tenant
    def get_all(self, tenant):
        query = {'tenant': tenant}
        return list(self.__collection.find(query))

    # Delete condition set by ID
    def delete_by_id(self, tenant, identifier):
        query = {
            '_id': ObjectId(identifier),
            'tenant': tenant
        }
        self.__collection.delete_one(query)
