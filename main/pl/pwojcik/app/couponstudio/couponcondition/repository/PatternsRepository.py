import json
#
#
# Repository defines function that reads static patterns from JSON file.
#
#


def read_patterns_from_file():
    try:
        with open('patterns.json') as json_file:
            return json.load(json_file)
    except FileNotFoundError as error:
        print("Could not open file: {}".format(error))
        return {}


PATTERNS = read_patterns_from_file()
