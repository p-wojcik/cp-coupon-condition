from pymongo import MongoClient

#
#
# Instructions responsible for setting up connection with MongoDB
#
#

MONGO_CONNECTION_STRING = 'mongodb://localhost:27017/coupons'
DATABASE_NAME = 'coupons'

mongo_client = MongoClient(MONGO_CONNECTION_STRING)
mongo_connector = mongo_client[DATABASE_NAME]
