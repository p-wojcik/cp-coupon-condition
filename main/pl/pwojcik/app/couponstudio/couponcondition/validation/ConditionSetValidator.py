from main.pl.pwojcik.app.couponstudio.couponcondition.exception.ConditionSetValidationException import \
    ConditionSetValidationException
from main.pl.pwojcik.app.couponstudio.couponcondition.repository.PatternsRepository import PATTERNS

#
#
# Class responsible for validating business requirements of Condition Set
#
#
PATTERN_IDS = list(map(lambda i: i['patternId'], PATTERNS))


class ConditionSetValidator:

    @staticmethod
    def validate(condition_set):
        if not condition_set:
            raise ConditionSetValidationException("Payload cannot be empty")
        if not condition_set.name:
            raise ConditionSetValidationException("Attribute 'name' cannot be empty")
        if not condition_set.tenant:
            raise ConditionSetValidationException("Attribute 'tenant' cannot be empty")
        if not condition_set.patterns:
            raise ConditionSetValidationException("Attribute 'patterns' cannot be empty")
        for pattern in condition_set.patterns:
            ConditionSetValidator.validate_pattern(pattern)

    @staticmethod
    def validate_pattern(pattern):
        if 'patternId' not in pattern:
            raise ConditionSetValidationException("Pattern attribute 'patternID' cannot be empty")
        if pattern['patternId'] not in PATTERN_IDS:
            raise ConditionSetValidationException("Pattern with given 'patternID' does not exist")
        if 'mapping' not in pattern:
            raise ConditionSetValidationException("Pattern attribute 'mapping' cannot be empty")
        ConditionSetValidator.validate_mapping(pattern['mapping'], pattern['patternId'])

    @staticmethod
    def validate_mapping(mapping, pattern_id):
        static_pattern = list(filter(lambda i: i['patternId'] == pattern_id, PATTERNS))[0]
        static_variables = eval(static_pattern['variables'])
        dynamic_keys = list(mapping.keys())
        if set(static_variables).intersection(set(dynamic_keys)) != set(static_variables):
            raise ConditionSetValidationException(
                "Pattern attribute 'mapping' does not match with pattern '{}' definition".format(pattern_id))
