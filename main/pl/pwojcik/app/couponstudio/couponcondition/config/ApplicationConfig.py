#
#
# Application's configuration variables.
#
#

MAIN_APP_NAME = '__main__'
APP_URL_PREFIX = '/cp-coupon-condition'
APP_PORT = 4400

# Constants
CP_TENANT_HEADER = 'cp-tenant'
