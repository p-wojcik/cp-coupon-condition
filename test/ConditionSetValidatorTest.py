import unittest2
from main.pl.pwojcik.app.couponstudio.couponcondition.config import ApplicationConfig
from main.pl.pwojcik.app.couponstudio.couponcondition.exception.ConditionSetValidationException import \
    ConditionSetValidationException
from main.pl.pwojcik.app.couponstudio.couponcondition.model.ConditionSet import ConditionSet
from main.pl.pwojcik.app.couponstudio.couponcondition.validation.ConditionSetValidator import ConditionSetValidator


class ConditionSetValidatorTest(unittest2.TestCase):

    def test_should_fail_when_entity_is_empty(self):
        entity = {}
        with self.assertRaises(ConditionSetValidationException):
            ConditionSetValidator.validate(entity)

    def test_should_fail_when_entity_has_no_name(self):
        entity = {
            "tenant": "tenant",
            "patterns": [
                {
                    "patternId": "FREE_PRODUCT",
                    "mapping": {
                        "x": 1
                    }
                }
            ]
        }
        condition_set = ConditionSet(**entity)
        with self.assertRaises(ConditionSetValidationException):
            ConditionSetValidator.validate(condition_set)

    def test_should_fail_when_entity_has_no_tenant(self):
        entity = {
            "name": "name",
            "patterns": [
                {
                    "patternId": "FREE_PRODUCT",
                    "mapping": {
                        "x": 1
                    }
                }
            ]
        }
        condition_set = ConditionSet(**entity)
        with self.assertRaises(ConditionSetValidationException):
            ConditionSetValidator.validate(condition_set)

    def test_should_fail_when_entity_has_no_patterns(self):
        entity = {
            "name": "name",
            "tenant": "tenant"
        }
        condition_set = ConditionSet(**entity)
        with self.assertRaises(ConditionSetValidationException):
            ConditionSetValidator.validate(condition_set)

    def test_should_fail_when_pattern_has_no_pattern_id(self):
        entity = {
            "name": "name",
            "tenant": "tenant",
            "patterns": [
                {
                    "mapping": {
                        "x": 1
                    }
                }
            ]
        }
        condition_set = ConditionSet(**entity)
        with self.assertRaises(ConditionSetValidationException):
            ConditionSetValidator.validate(condition_set)

    def test_should_fail_when_pattern_has_invalid_pattern_id(self):
        entity = {
            "name": "name",
            "tenant": "tenant",
            "patterns": [
                {
                    "patternId": "invalid",
                    "mapping": {
                        "x": 1
                    }
                }
            ]
        }
        condition_set = ConditionSet(**entity)
        with self.assertRaises(ConditionSetValidationException):
            ConditionSetValidator.validate(condition_set)

    def test_should_fail_when_pattern_has_no_mapping(self):
        entity = {
            "name": "name",
            "tenant": "tenant",
            "patterns": [
                {
                    "patternId": "FREE_PRODUCT"
                }
            ]
        }
        condition_set = ConditionSet(**entity)
        with self.assertRaises(ConditionSetValidationException):
            ConditionSetValidator.validate(condition_set)


if __name__ == ApplicationConfig.MAIN_APP_NAME:
    unittest2.main()
